#include"future_command_table.h"
#include<string.h>
#include<unistd.h>
#include<stdio.h>

void future_command_table_init(future_command_table_type* table){
	table->future_pipe_table[0] = table->future_command_number ;
	table->future_pipe_table[1] = table->pipe_fd_number ;
	table->size = 0 ;
	table->next_available_space = 0 ;
}

void add_into_future_command_table(future_command_table_type* table, const unsigned command_number, const unsigned pipefd_number){
	if(table->size==2048){
		write(2,"ERROR: table full.\n", strlen("ERROR: table full.\n")) ;
		return ;
	}


	unsigned pos = table->next_available_space ;
	unsigned start = mod(table->next_available_space - table->size,2048) ;
	while(pos != start && command_number < table->future_command_number[mod(pos-1,2048)]){
		unsigned prev = mod(pos-1,2048) ;
		table->future_command_number[pos] = table->future_command_number[prev] ;
		table->pipe_fd_number[pos] = table->pipe_fd_number[prev] ;
		pos = prev ;
	}
	table->future_command_number[pos] = command_number ;
	table->pipe_fd_number[pos] = pipefd_number ;

	table->size++ ;
	table->next_available_space = mod(table->next_available_space+1,2048) ;

}

int check_future_command_table(future_command_table_type* table, unsigned current_round, unsigned* pipefd_number){
	if(table->size==0) return 0 ;
	
	unsigned start = mod(table->next_available_space - table->size,2048) ;
	if(table->future_command_number[start]!=current_round) return 0 ;
	else{
		*pipefd_number = table->pipe_fd_number[start] ;
		table->size-- ;
		return 1 ;
	}
	
}

unsigned mod(int a, int b){
	int r = a%b ;
	if(r<0) r = r+b ;
	return r ;
}

void print_table(future_command_table_type* table){
	int i ;
	unsigned start = table->next_available_space - table->size ;
	for(i = 0 ; i<table->size ; i++){
		printf("%d\n",table->future_command_number[i]) ;
	}
}
